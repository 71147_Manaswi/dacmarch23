/*20.Write a program to convert:
a. long value into String
b. long value into Long instance.
c. String instance into Long instance.
d. long value into binary, octal and hexadecimal string.*/
class Q20 {
    public static void main(String args[])
    {
        Long s = 3535L;
       String str= 	String.valueOf(s);
    System.out.println(s+" Long value into String "+ str);
    Long s1=Long.valueOf(s);
    System.out.println(s+" Long value into Long instance "+ s1);
    Long s2=Long.valueOf(str);
    System.out.println(str+" String instance into Long instance "+ s2);
    System.out.println(s+" Long value into binary "+ Long.toBinaryString(s));
    System.out.println(s+" long value into octal "+ Long.toOctalString(s));
    System.out.println(s+" long value into Hexadecimal "+Long.toHexString(s));
    }

}