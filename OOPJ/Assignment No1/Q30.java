/*Write a program to find minimum and maximum number as well as
to add two double numbers using methods of Double.*/
import java.util.Scanner;
class Q30
{
public static  void main(String[] args)
 {
     Scanner sc= new Scanner(System.in);
     System.out.println("Enter two number");

Double a= sc.nextDouble();
Double b=sc.nextDouble();

System.out.println("Maximum Number is "+Double.max( a,  b));
System.out.println("Minimum Number is "+Double.min( a,  b));
System.out.println("Addition of two number is "+Double.sum( a,b));
}
}