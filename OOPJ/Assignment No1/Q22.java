/*22. Write a program to find minimum and maximum number as well as
to add two long numbers using methods of Long.*/
import java.util.Scanner;
class Q22
{
public static  void main(String[] args)
 {
     Scanner sc= new Scanner(System.in);
     System.out.println("Enter two number");

Long a= sc.nextLong();
Long b=sc.nextLong();

System.out.println("Maximum Number is "+Long.max( a,  b));
System.out.println("Minimum Number is "+Long.min( a,  b));
System.out.println("Addition of two number is "+Long.sum( a,b));
}
}

