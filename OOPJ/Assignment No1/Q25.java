/*25.Write a program to convert state of Float instance into byte,
short, int, long, float and double.*/
class Q25
{
    public static void main(String args[])
     {
        float s = 25f;
       Float s1=Float.valueOf(s);
        System.out.println(s+" int value conver into instance "+ s1);
        
        System.out.println("Byte : "+s1.byteValue());
        System.out.println("short :"+s1.shortValue());
        System.out.println("int : "+s1.intValue());
        System.out.println("long : "+s1.longValue());
        System.out.println("float : "+s1.floatValue());
        System.out.println("double : "+s1.doubleValue());


     }
}
