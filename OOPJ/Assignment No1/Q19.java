/*19.Write a program to perform below operations on long type to get:
 a. The number of bits used to represent a long value
  b. The number of bytes used to represent a long value 
  c. The minimum value a long 
d. The maximum value a long*/
class Q19 {
    public static void main (String args[])
    {
        
       System.out.println("The number of bits used to represent a Long value is "+Long.SIZE );
       System.out.println("The number of bytes used to represent a Long value " +Long.BYTES);
        System.out.println("The minimum value a Long is "+Long.MIN_VALUE );
        System.out.println("The maximum value a Long is " +Long.MAX_VALUE);
    }

}