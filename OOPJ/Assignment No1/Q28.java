/*28.Write a program to convert:
a. double value into String
b. double value into Double instance.
c. String instance into Double instance.
d. double value into binary, octal and hexadecimal
string(Note: Here you can use doubleToLongBits() method
along with methods of Long class).*/

 class Q28 {
    public static void main(String args[])
    {
        double s = 3535;
       String str= 	String.valueOf(s);
    System.out.println(s+" double value into String "+ str);
    Double s1= Double.valueOf(s);
    System.out.println(s+" double value into Double instance "+ s1);
    Double s2=Double.valueOf(str);
    System.out.println(str+" string instance into Double instance "+ s2);
    
   long ans=Double.doubleToLongBits(s);
    System.out.println(s+" double value into binary "+ Long.toBinaryString(ans));
    System.out.println(s+" double value into octal "+ Long.toOctalString(ans));
    System.out.println(s+" double value into Hexadecimal "+Long.toHexString(ans));
    }


}