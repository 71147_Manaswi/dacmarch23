/*21.Write a program to convert state of Long instance into byte,
short, int, long, float and double.*/
class Q21
{
    public static void main(String args[])
     {
        Long s = 9000L;
        Long s1= Long.valueOf(s);
        System.out.println(s+" int value conver into instance "+ s1);
        
        System.out.println("Byte : "+s1.byteValue());
        System.out.println("short :"+s1.shortValue());
        System.out.println("int : "+s1.intValue());
        System.out.println("long : "+s1.longValue());
        System.out.println("float : "+s1.floatValue());
        System.out.println("double : "+s1.doubleValue());


     }
}
