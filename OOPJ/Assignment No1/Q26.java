
/*Write a program to find minimum and maximum number as well as
to add two float numbers using methods of Float.
*/
import java.util.Scanner;
class Q26
{
public static  void main(String[] args)
 {
     Scanner sc= new Scanner(System.in);
     System.out.println("Enter two number");

Float a= sc.nextFloat();
Float b=sc.nextFloat();

System.out.println("Maximum Number is "+Float.max( a,  b));
System.out.println("Minimum Number is "+Float.min( a,  b));
System.out.println("Addition of two number is "+Float.sum( a,b));
}
}
