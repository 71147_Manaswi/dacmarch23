/*18. Write a program to find minimum and maximum number as well as
to add two integer numbers using methods of Integer.*/
import java.util.Scanner;
class Q18
{
public static  void main(String[] args)
 {
     Scanner sc= new Scanner(System.in);
     System.out.println("Enter two number");

int a= sc.nextInt();
int b=sc.nextInt();

System.out.println("Maximum Number is "+Integer.max( a,  b));
System.out.println("Minimum Number is "+Integer.min( a,  b));
System.out.println("Addition of two number is "+Integer.sum( a,b));
}
}
